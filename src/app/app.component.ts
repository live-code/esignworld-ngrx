import { Component, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { addItem, addItemSuccess, deleteItem, loadItems } from './store/items.actions';
import { Observable, Subscription } from 'rxjs';
import { Item } from './model/item';
import { AppState } from './app.module';
import { NgForm } from '@angular/forms';
import { getItems, getItemsErrors, totalItems } from './store/items.selectors';
import { Actions, ofType } from '@ngrx/effects';
import { setFilter } from './store/filters.actions';

@Component({
  selector: 'app-root',
  template: `
    <div *ngIf="itemsError$ | async">Errore!</div>
    <form #f="ngForm" (submit)="addItemHandler(f.value)">
      <input type="text" ngModel name="name">
    </form>

    <hr>
    <input type="text" (input)="filterByNameHandler($event)">
    TOTAL: {{totalItemsr$ | async}}
    <li *ngFor="let item of items$ | async">
      {{item.name}}
      <button (click)="deleteHandler(item)">delete</button>
    </li>

    <button (click)="loadMoreHandler()">load more</button>
  `,
})
export class AppComponent {
  @ViewChild('f') form: NgForm;
  items$: Observable<Item[]> = this.store.pipe(select(getItems));
  itemsError$: Observable<boolean> = this.store.pipe(select(getItemsErrors));
  totalItemsr$: Observable<number> = this.store.pipe(select(totalItems));

  constructor(private store: Store<AppState>, actions$: Actions) {
    actions$
      .pipe(
        ofType(addItemSuccess)
      )
      .subscribe(() => this.form.reset());
    store.dispatch(loadItems());
  }

  addItemHandler(item: Item): void {
    this.store.dispatch(addItem({ item }))
  }

  deleteHandler(item: Item): void {
    this.store.dispatch(deleteItem({ id: item.id}))
  }

  loadMoreHandler(): void {
    this.store.dispatch(loadItems())
  }

  filterByNameHandler(event: Event): void {
    const filterName = (event.currentTarget as HTMLInputElement).value
    this.store.dispatch(setFilter({ filterName }))
  }
}
