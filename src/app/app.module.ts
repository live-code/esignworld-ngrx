import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionReducer, ActionReducerMap, combineReducers, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './store/items.effects';
import { HttpClientModule } from '@angular/common/http';
import { listReducer, ListState } from './store/items.reducer';

export interface ConfigState {
  theme: string;
  config: number;
}

export const configReducer: ActionReducer<ConfigState> = combineReducers({
  theme: () => 'dark',  // can be a reducer
  config: () => 123     // can be a reducer
});


export interface AppState {
  items: ListState;
  config: ConfigState;
}

const reducers: ActionReducerMap<AppState> = {
  items: listReducer,
  config: configReducer
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 20,
    }),
    /*StoreRouterConnectingModule.forRoot({
      // routerState: RouterState.Minimal
    }),*/
    EffectsModule.forRoot([ItemsEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
