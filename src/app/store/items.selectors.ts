import { AppState } from '../app.module';
import { createSelector } from '@ngrx/store';
import { Item } from '../model/item';

export const getItemsErrors = ((state: AppState) => state.items.error);
export const getItemsList = ((state: AppState) => state.items.list);
// export const getItemsFilter = ((state: AppState) => state.items.filter.filterName);
export const getItemsFilter = ((state: AppState) => '');

export const getItems = createSelector(
  getItemsList,
  getItemsFilter,
  (list: Item[], filter: string) => list.filter(item => {
    return item.name.toLowerCase().includes(filter.toLowerCase())
  })
);

export const totalItems = createSelector(
  getItems,
  (list: Item[]) => list.length
);
/*

export const getItemsSIMPLE = ((state: AppState) => {
  return state.items.list.filter(item => {
    return item.name.toLowerCase().includes(state.items.filterName.toLowerCase())
  });
});
export const getItemsError = ((state: AppState) => state.items.error);
*/
