import { createReducer, on } from '@ngrx/store';
import { Item } from '../model/item';
import {
  addItem,
  addItemFailed,
  addItemSuccess,
  deleteItem,
  deleteItemFailed,
  deleteItemSuccess,
  loadItemsSuccess,
} from './items.actions';

export interface ListState {
  list: Item[];
  error: boolean;
}

const initialState: ListState = {
  list: [],
  error: false,
}

export const listReducer = createReducer(
  initialState,
  on(loadItemsSuccess, (state, action) => ({ ...state, list: [...action.items]})),

  on(addItem, (state, action) => ({ ...state, error: false})),
  on(addItemSuccess, (state, action) => ({ ...state, list: [...state.list, action.item] })),
  on(addItemFailed, (state, action) => ({ ...state, error: true })),

  on(deleteItem, (state, action) => ({ ...state, error: false})),
  on(deleteItemSuccess, (state, action) => ({...state, list: state.list.filter(item =>  item.id !== action.id) })),
  on(deleteItemFailed, (state, action) => ({ ...state, error: true})),

);
