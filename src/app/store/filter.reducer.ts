import { createReducer, on } from '@ngrx/store';
import { Item } from '../model/item';
import { setFilter } from './filters.actions';

export interface FilterState {
  filterName: string;
}

const initialState: FilterState = {
  filterName: '',
};

export const filtersReducer = createReducer(
  initialState,
  on(setFilter, (state, action) => ({...state, filterName: action.filterName}))
);
