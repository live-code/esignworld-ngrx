import { createAction, props } from '@ngrx/store';

export const setFilter = createAction(
  '[items]: filter',
  props<{ filterName: string }>()
)
