import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { catchError, concatMap, delay, map, mergeMap, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import {
  addItem,
  addItemFailed,
  addItemSuccess,
  deleteItem, deleteItemFailed,
  deleteItemSuccess,
  loadItems,
  loadItemsFailed,
  loadItemsSuccess
} from './items.actions';
import { Item } from '../model/item';
import { fromEvent, of } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { DOCUMENT } from '@angular/common';

@Injectable()
export class ItemsEffects {

  log$ = createEffect(() => this.actions$.pipe(
    withLatestFrom(this.store.pipe( select (state => state) )),
    // tap(data => console.log(data[0], data[1])),
    switchMap(([action, state]) => this.http.post<any>('https://jsonplaceholder.typicode.com/users', { action, state }))
  ), { dispatch: false});

  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(loadItems),
    switchMap(
      () => this.http.get<Item[]>('https://jsonplaceholder.typicode.com/users')
        .pipe(
          map(items => loadItemsSuccess({ items })),
          catchError(() => of(loadItemsFailed()))
        )
    )
  ));

  addItems$ = createEffect(() => this.actions$.pipe(
    ofType(addItem),
    concatMap(
      action => this.http.post<Item>('https://jsonplaceholder.typicode.com/users', action.item)
        .pipe(
          delay(1000),
          map(item => addItemSuccess({ item })),
          catchError(() => of(addItemFailed()))
        )
    )
  ));

  deleteItems$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItem),
    mergeMap(
      action => this.http.delete('https://xjsonplaceholder.typicode.com/users/' + action.id)
        .pipe(
          map(() => deleteItemSuccess({ id: action.id })),
          catchError(() => of(deleteItemFailed()))
        )
    )
  ));

  constructor(private actions$: Actions, private http: HttpClient, private store: Store<AppState>) {
  }
}
